# Security Policy

## Supported Versions

<!-- Use this section to tell people about which versions of your project are
currently being supported with security updates. -->

The application is compatible with the following versions of Dart

| Version | Supported          |
| ------- | ------------------ |
| 1-2   | :white_check_mark: |
| 3+   | :x:                | 

## Reporting a Vulnerability

<!-- Use this section to tell people how to report a vulnerability.

Tell them where to go, how often they can expect to get an update on a
reported vulnerability, what to expect if the vulnerability is accepted or
declined, etc. -->

If you find any error or vulnerabilities in the code, create in issue so i can fix it.
Or even better, create a pull request