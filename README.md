WIP

<p align="center">
<!-- <img src="https://www.slashdesign.nl/wp-content/uploads/2022/07/LOGO-SPINE-DARK-BG.png" alt="Paris" width="360"> -->
</p>
<h1 align="center">Slash Design</h1>
<p align="center"><i>Developed with ❤️ by <a href="https://www.slashdesign.nl">Slash Design</a></i></p>
<div align="center">
  <a href="https://github.com/lazy-demon/slash-design/stargazers"><img src="https://img.shields.io/github/stars/lazy-demon/slash-design" alt="Stars Badge"/></a>
  <a href="https://github.com/lazy-demon/slash-design/network/members"><img src="https://img.shields.io/github/forks/lazy-demon/slash-design" alt="Forks Badge"/></a>
  <a href="https://github.com/lazy-demon/slash-design/pulls"><img src="https://img.shields.io/github/issues-pr/lazy-demon/slash-design" alt="Pull Requests Badge"/></a>
  <a href="https://github.com/lazy-demon/slash-design/issues"><img src="https://img.shields.io/github/issues/lazy-demon/slash-design" alt="Issues Badge"/></a>
  <a href="https://github.com/lazy-demon/slash-design/graphs/contributors"><img alt="GitHub contributors" src="https://img.shields.io/github/contributors/lazy-demon/slash-design?color=2b9348"></a>
  <a href="https://img.shields.io/badge/license-MIT-green"><img src="https://img.shields.io/badge/license-MIT-green" alt="License Badge"/></a>
</div>
<br>
<!-- <p align="center"><i>Loved the project? Please visit <a href="https://www.slashdesign.nl/">slashdesign.nl</a></i></p>
<br> -->

slash-design is a powerful and confidential digital forensics and online investigation tool designed exclusively for our organization's needs. It serves as a critical asset for our investigative teams, enabling in-depth analysis and insights from various online platforms and data sources.

## Tools

💾  Docker: Containerization platform for seamless app development. <br>
🛠️  dart:   <br>
🛠️  flutter:   <br> 
🛠️  Serverpod:   <br>
🐋  PostgreSQL: Powerful open-source relational database management system (RDBMS). <br>
🧮  Visual Studio Code: Versatile multi-languege code editor with support for plugins and Dev Containers.<br> 

## Getting Started

The setup for slash-design has been simplified by using Docker. You only need to install docker and the Dev Container will configure itself!

### Prerequisites

* Docker

   ```sh
   winget install -e --id Docker.DockerDesktop
  ```

* Visual Studio Code

   ```sh
   winget install -e --id Microsoft.VisualStudioCode
  ```

### Setup
1. Open Visual Studio Code
2. Clone this repo. VSC will ask you if you want to re-open the project in a Dev Container.
2. Click on 🞂 to start the client and database.
3. Open localhost:8080 as asked by VSC.

## Roadmap

* [x] Finish README 

See the [open issues](https://github.com/lazy-demon/slash-design/issues) for a full list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

### Tags

| Type     | Description                                       |
|----------|---------------------------------------------------|
| init     | Initial commit                                    | 
| feat     | A new feature                                     |
| fix      | A bug fix                                         |
| docs     | Documentation only changes                        |
| style    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| refactor | A code change that neither fixes a bug nor adds a feature |
| perf     | A code change that improves performance           |
| test     | Adding missing tests                              |
| chore    | Changes to the build process or auxiliary tools and libraries such as documentation generation |

## Contact

| Name            | Email                             |
|-----------------|-----------------------------------|
| Jeffrey Boone   | <jefdayt@gmail.com>      |
| Alysia G.    | bleh      |
